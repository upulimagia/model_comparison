import numpy as np
from statsmodels.stats import contingency_tables


def mcnemar_test_from_dataframe(model_one_prediction, model_two_prediction):
    cross_table = calculate_cross_table(model_one_prediction, model_two_prediction)
    return contingency_tables.mcnemar(cross_table, exact=True)


# TODO: (upul)
# I think we can replace this ugly method with
# an inbuilt function that comes with pandas or statsmodels.
def calculate_cross_table(df_1, df_2):
    assert len(df_1) == len(df_2)

    cross_table = np.zeros((2, 2))

    for i in range(len(df_1)):
        df_1_row_i = df_1.iloc[i]
        df_2_row_i = df_2.iloc[i]

        actual = df_1_row_i['real_y']
        model_1_pred = df_1_row_i['pred_y']
        model_2_pred = df_2_row_i['pred_y']

        if (model_1_pred == actual) and (model_2_pred == actual):
            cross_table[0][0] += 1
        elif (model_1_pred == actual) and (model_2_pred != actual):
            cross_table[0][1] += 1
        elif (model_1_pred != actual) and (model_2_pred == actual):
            cross_table[1][0] += 1
        else:
            cross_table[1][1] += 1
    return cross_table
