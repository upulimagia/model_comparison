import click
import pandas as pd
from imagia.model_comparison.mcnemar import mcnemar_test_from_dataframe


@click.group()
def run():
    pass


@run.command()
@click.option('--model-one-predictions', '-m1', required=True,
              help='predictions obtained from the first model')
@click.option('--model-two-predictions', '-m2', required=True,
              help='predictions obtained from the second model')
def mcnemar(model_one_predictions, model_two_predictions):
    df_1 = pd.read_csv(model_one_predictions)
    df_2 = pd.read_csv(model_two_predictions)

    print(mcnemar_test_from_dataframe(df_1, df_2))


def main():
    run()


if __name__ == '__main__':
    main()
