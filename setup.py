import os
from setuptools import setup, find_packages

version = os.environ.get('IMAGIA_VERSION', '0.0.0')

LOCAL_REQUIRES = [

]

REQUIRES = [
    'statsmodels',
    'click',
    'pandas',
    'numpy'
]

EXTRAS_REQUIRE = {}

INSTALL_REQUIRES = LOCAL_REQUIRES + REQUIRES


def setup_package():
    setup(
        name='imagia.model_comparison',
        version=version,
        author="OI Team",
        url='http://www.imagia.com/',
        description="",
        packages=find_packages(exclude=['tests', '*.tests', '*.tests.*']),
        install_requires=INSTALL_REQUIRES,
        extras_require=EXTRAS_REQUIRE,
        package_dir={'model_comparison': 'model_comparison'},
        include_package_data=True,
        entry_points={
            'console_scripts': [
                'imagia.model_comparison = imagia.model_comparison.main:main'
            ]
        }
    )


if __name__ == '__main__':
    setup_package()
