## Model Comparison

## How to Install?
1. Crean a new virtual environment  `virtualenv -p python3.6 venv`
2. Activate the environment source `./venv/bin/activate`
3. Install requirements `pip install -r requirements.txt`
4. Locally install the package `pip install -e .`

### Run a Sample 
`imagia.model_comparison mcnemar -m1 ./sample_data/sample_model_1.csv -m2 ./sample_data/sample_model_2.csv`